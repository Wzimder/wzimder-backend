namespace Backend.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class zmiany : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Photos", "Url");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Photos", "Url", c => c.String());
        }
    }
}
