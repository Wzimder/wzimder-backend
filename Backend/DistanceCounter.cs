﻿using System;
using System.Device.Location;
using System.Globalization;

namespace Backend
{
    public class DistanceCounter
    {
        //metoda do liczenia odleglosci
        //system.location.device.portable biblioteka
        //zwraca double w kilometrach, zaokraglajac
        public static double GetDistance(string location1, string location2)
        {
            if (location1.Contains("."))
            {
                location1 = location1.Replace('.', ',');
            }

            if (location2.Contains("."))
            {
                location2 = location2.Replace('.', ',');
            }

            string lat1 = location1.Substring(0, location1.IndexOf(';'));
            string lng1 = location1.Substring(location1.IndexOf(';') + 1);

            string lat2 = location2.Substring(0, location2.IndexOf(';'));
            string lng2 = location2.Substring(location2.IndexOf(';') + 1);

            var culture = CultureInfo.CreateSpecificCulture("pl-PL");

            var sCoord = new GeoCoordinate(Convert.ToDouble(lat1, culture), Convert.ToDouble(lng1, culture));
            var eCoord = new GeoCoordinate(Convert.ToDouble(lat2, culture), Convert.ToDouble(lng2, culture));

            return Math.Round((sCoord.GetDistanceTo(eCoord) / 1000), 0);
        }
    }
}