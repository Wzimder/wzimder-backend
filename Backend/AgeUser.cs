﻿using System;

namespace Backend
{
    public class AgeUser
    {
        public static int Convert(DateTime dtb)
        {
            int age;
            DateTime dtn = DateTime.Now;
            DateTime tmp = new DateTime(dtn.Year, dtb.Month, dtb.Day);

            if (tmp > dtn)
            {
                age = dtn.Year - dtb.Year - 1;
            }
            else
            {
                age = dtn.Year - dtb.Year;
            }

            return age;
        }
    }
}