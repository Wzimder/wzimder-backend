﻿namespace Backend.Models
{
    // Models returned by AccountController actions.

    public class LoginViewModel
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string userName { get; set; }        
        public string issued { get; set; } // .issued
        public string expires { get; set; } // .expires
    }
}
