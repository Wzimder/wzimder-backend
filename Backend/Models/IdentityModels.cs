﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Backend.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Location { get; set; }
        public Gender Gender { get; set; }
        public string Description { get; set; }
        public DateTime? Birthdate { get; set; }
        public virtual ICollection<Photo> Photos { get; set; }
        public virtual SearchOptions SearchOptions { get; set; }
        [InverseProperty("Recipient")]
        public virtual ICollection<Message> MessagesReceived { get; set; }
        [InverseProperty("Sender")]
        public virtual ICollection<Message> MessagesSent { get; set; }
        public int ProfilePhoto { get; set; }
        [InverseProperty("User1")]
        public virtual ICollection<Pair> User1Pairs { get; set; }
        [InverseProperty("User2")]
        public virtual ICollection<Pair> User2Pairs { get; set; }
    }

    public enum Gender
    {
        Male,
        Female,
        Unknown
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public virtual DbSet<Pair> Pairs { get; set; }
        public virtual DbSet<Photo> Photos { get; set; }
        public virtual DbSet<SearchOptions> SearchOptions { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }        
    }
}