﻿using System;

namespace Backend.Models
{
    public class Message
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public virtual ApplicationUser Sender  { get; set; }
        public virtual ApplicationUser Recipient { get; set; }
        public bool Unread { get; set; }
        public DateTime? TimeUtc { get; set; }
    }
}