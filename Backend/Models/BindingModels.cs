﻿using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class MessageBindingModel
    {
        [Required]
        public string Text { get; set; }
        [Required]
        public string RecipientId { get; set; }
    }

    public class SearchOptionsBindingModel
    {
        [Required]
        public int MinAge { get; set; }
        [Required]
        public int MaxAge { get; set; }
        [Required]
        public Gender Gender { get; set; }
        [Required]
        public int MaxDistanceKM { get; set; }
    }

    public class LocationBindingModel
    {
        [Required]
        public string Location { get; set; }
    }

    public class PairBindingModel
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        public Status Status { get; set; }
    }

    public class DescriptionBindingModel
    {
        [Required]
        public string Description { get; set; }
    }
}