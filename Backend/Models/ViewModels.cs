﻿using System;
using System.Collections.Generic;

namespace Backend.Models
{
    public class MessageViewModel
    {
        public string Text { get; set; }
        public string SenderId { get; set; }
        public DateTime? TimeUtc { get; set; }
    }

    public class SearchOptionsViewModel
    {
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public Gender Gender { get; set; }
        public int MaxDistanceKM { get; set; }
    }

    public class UserViewModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public DateTime? Birthdate { get; set; }
        public int Photo { get; set; }        
    }

    public class ExtendedUserViewModel : UserViewModel
    {
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string Description { get; set; }
        //public IEnumerable<int> Photos { get; set; }
    }

    public class PairUserViewModel : ExtendedUserViewModel
    {
        public string Location { get; set; }
    }

    public class AccountInfoViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? Birthdate { get; set; }
        public Gender Gender { get; set; }
        public string Description { get; set; }
        public int ProfilePhotoId { get; set; }
    }
}