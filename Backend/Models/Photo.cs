﻿namespace Backend.Models
{
    public class Photo
    {
        public int Id { get; set; }
        public virtual ApplicationUser User { get; set; }
        public byte[] Content { get; set; }
    }
}