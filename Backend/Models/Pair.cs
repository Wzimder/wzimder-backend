﻿namespace Backend.Models
{
    public class Pair
    {
        public int Id { get; set; }
        public virtual ApplicationUser User1 { get; set; }
        public Status User1Status { get; set; }
        public virtual ApplicationUser User2 { get; set; }
        public Status User2Status { get; set; }
    }

    public enum Status
    {
        Like,
        DontLike,
        Unknown
    }
}