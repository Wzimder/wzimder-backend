﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Backend.Models;
using Microsoft.AspNet.Identity;

namespace Backend.Controllers
{
    [Authorize]
    public class UsersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Users
        /// <summary>
        /// pobranie nowych osob (max 30)
        /// </summary>
        /// <returns></returns>
        [Route("api/users")]
        public ICollection<UserViewModel> GetUsers()
        {
            var resultList = new List<UserViewModel>();

            var id = User.Identity.GetUserId();
            var user = db.Users.Find(id);
            var so = user.SearchOptions;
            if (user.Location == null)
            {
                return resultList;
            }

            bool flaga;

            foreach (var el in db.Users.ToList().Except(new[] { user }))
            {
                /*
                    WYSZUKIWANIE PODZIELONE NA DWA ETAPY. 
                    W PIERSZYW ETAPIE WYBIERAM Z POŚRÓD WSZYSTKICH TYCH KTÓRZY PASUJĄ 
                    DO KRYTERIÓW WYSZUKIWANIA. PONIŻSZY IF TO REALIZUJE.
                */                
                if (el.Location == null)
                {
                    continue;
                }

                if ((el.Gender == so.Gender) &&
                    (so.MinAge <= AgeUser.Convert((DateTime)el.Birthdate) && so.MaxAge >= AgeUser.Convert((DateTime)el.Birthdate)) &&
                    (DistanceCounter.GetDistance(user.Location, el.Location) <= so.MaxDistanceKM))
                {
                    flaga = true;

                    /*
                        W DRUGIM ETAPIE UWZGLĘDNIAM PARY. A WIĘC UŻYTKOWNIK (user) MA DWIE LISTY PAR:
                        User1Pairs - NA PODSTAWIE TEJ LISTY ELEMINUJE OSOBĘ KRÓRA PRZESZŁA POWYŻSZY if A ZNAJDZIE SIĘ NA NIEJ, NO BO PO CO DWA RAZY OCENIAĆ=)
                        User2Pairs - NA PODSTAWIE TEJ LISTY ELEMINUJE OSOBĘ KTÓRA DAŁA DontLike, NO BO RACZEJ NIE CHCE NAS=) + pomijamy jesli juz ocenilismy
                     */
                    
                    foreach (var os in user.User1Pairs.ToList())
                    {
                        if (el.Id == os.User2.Id)
                        {
                            flaga = false;
                            break;
                        }
                    }

                    if (flaga)
                    {
                        foreach (var os in user.User2Pairs.ToList())
                        {
                            if (el.Id == os.User1.Id && (os.User1Status == Status.DontLike || os.User2Status != Status.Unknown))
                            {
                                flaga = false;
                                break;
                            }
                        }
                    }

                    if (flaga)
                    {
                        var uvm = new UserViewModel();
                        uvm.Id = el.Id;
                        uvm.FirstName = el.FirstName;
                        uvm.Birthdate = el.Birthdate;
                        uvm.Photo = el.ProfilePhoto;
                        resultList.Add(uvm);
                    }
                }
            }

            return resultList.Take(30).ToList();
        }

        // GET: api/Users/5
        /// <summary>
        /// pobranie dokladniejszych danych innego uzytkownika
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/users/{id}")]
        public ExtendedUserViewModel GetUser(string id)
        {
            var user = db.Users.Find(id);
            var euvm = new ExtendedUserViewModel();

            euvm.Id = user.Id;
            euvm.FirstName = user.FirstName;
            euvm.LastName = user.LastName;
            euvm.Birthdate = user.Birthdate;
            euvm.Gender = user.Gender;
            euvm.Photo = user.ProfilePhoto;
            euvm.Description = user.Description;
            //euvm.Photos = user.Photos.Select(p => p.Id);            

            return euvm;
        }        
                        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(string id)
        {
            return db.Users.Count(e => e.Id == id) > 0;
        }
    }
}