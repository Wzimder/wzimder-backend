﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Backend.Models;
using Microsoft.AspNet.Identity;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Backend.Controllers
{
    [Authorize]
    public class PhotosController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Photos
        /// <summary>
        /// pobranie własnych zdjęć (id)
        /// </summary>
        /// <returns></returns>
        //public ICollection<int> GetPhotos()
        //{
        //    string userId = User.Identity.GetUserId();
        //    var photos = db.Photos
        //        .Where(p => p.User.Id == userId)
        //        .Select(p => p.Id);
        //    return photos.ToList();
        //}

        /// <summary>
        /// zwrocenie zdjecia o danym id
        /// </summary>
        [AllowAnonymous]
        public HttpResponseMessage GetPhoto(int id)
        {
            Photo photo = db.Photos.Find(id);
            if (photo == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(photo.Content);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg"); // TODO

            return result;
        }

        // POST: api/photos
        /// <summary>
        /// dodanie zdjęcia, TYLKO multipart/form-data, zdjecie automatycznie ustawiane jest jako zdjecie profilowe, zapytanie MAX 2 MB
        /// </summary>
        public async Task<IHttpActionResult> PostPhoto()
        {
            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

                string userId = User.Identity.GetUserId();

                var provider = new MultipartMemoryStreamProvider();
                await Request.Content.ReadAsMultipartAsync(provider);

                var file = provider.Contents.FirstOrDefault();
                if (file != null)
                {
                    var data = await file.ReadAsByteArrayAsync();
                    var user = db.Users.Find(userId);
                    var oldPhotoId = user.ProfilePhoto;

                    var photo = new Photo();
                    photo.Content = data;
                    photo.User = user;
                    db.Photos.Add(photo);
                    db.SaveChanges();
                    user.ProfilePhoto = photo.Id;

                    var oldPhoto = db.Photos.Find(oldPhotoId);
                    if (oldPhoto != null)
                    {
                        db.Photos.Remove(oldPhoto);
                    }

                    db.SaveChanges();
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotAcceptable);
                }

                return Ok();
            }
            catch (HttpResponseException e)
            {
                throw e;
            }
            catch (System.Exception)
            {
                var res = new HttpResponseMessage(HttpStatusCode.BadRequest);
                res.Content = new StringContent("Problem z wyslaniem zdjecia. Sprobuj jeszcze raz. Mozliwe problemy: zbyt duzy rozmiar pliku, plik uszkodzony, zly format pliku");
                throw new HttpResponseException(res);
            }
        }

        // DELETE: api/photos/5
        /// <summary>
        /// usuniecie wlasnego zdjecia
        /// </summary>
        //public IHttpActionResult DeletePhoto(int id)
        //{
        //    Photo photo = db.Photos.Find(id);
        //    if (photo != null && photo.User.Id == User.Identity.GetUserId())
        //    {
        //        db.Photos.Remove(photo);
        //        db.SaveChanges();
        //        return Ok();
        //    }
        //    return NotFound();
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}