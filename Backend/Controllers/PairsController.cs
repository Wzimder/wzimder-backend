﻿using Backend.Models;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;

namespace Backend.Controllers
{
    [Authorize]
    [Route("api/pairs")]
    public class PairsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/pairs
        /// <summary>
        /// pobranie wszystkich użytkowników z którymi utworzono parę
        /// </summary>
        /// <returns></returns>
        public ICollection<PairUserViewModel> GetMyPairs()
        {
            ApplicationUser             curUser;
            List<PairUserViewModel>     pairUserList = new List<PairUserViewModel>();
            
            // zalogowany użytkownik:
            curUser = db.Users.Find(User.Identity.GetUserId());

            // pary, w których zalogowany użytkownik jest na pozycji pierwszej
            var q = db.Pairs.Where(p => p.User1.Id == curUser.Id && p.User1Status == Status.Like && p.User2Status == Status.Like);

            foreach (var p in q.ToList())
            {
                pairUserList.Add(new PairUserViewModel
                {
                    Id = p.User2.Id,
                    FirstName = p.User2.FirstName,
                    Birthdate = p.User2.Birthdate,
                    Photo = p.User2.ProfilePhoto,
                    LastName = p.User2.LastName,
                    Gender = p.User2.Gender,
                    Description = p.User2.Description,
                    //Photos = p.User2.Photos.Select(x => x.Id),
                    Location = p.User2.Location
                });
            }

            // pary, w których zalogowany użytkownik jest na pozycji drugiej
            q = db.Pairs.Where(p => p.User2.Id == curUser.Id && p.User1Status == Status.Like && p.User2Status == Status.Like);

            foreach (var p in q.ToList())
            {
                pairUserList.Add(new PairUserViewModel
                {
                    Id = p.User1.Id,
                    FirstName = p.User1.FirstName,
                    Birthdate = p.User1.Birthdate,
                    Photo = p.User1.ProfilePhoto,
                    LastName = p.User1.LastName,
                    Gender = p.User1.Gender,
                    Description = p.User1.Description,
                    //Photos = p.User1.Photos.Select(x => x.Id),
                    Location = p.User1.Location
                });
            }

            return pairUserList;
        }

        // POST: api/pairs
        /// <summary>
        /// polubienie lub odrzucenie osoby
        /// </summary>     
        public IHttpActionResult PostPair(PairBindingModel pair)
        {            
            ApplicationUser     curUser;
            ApplicationUser     user2;
            Pair                retPair;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            curUser = db.Users.Find(User.Identity.GetUserId());
            user2 = db.Users.Find(pair.UserId);

            if (user2 != null) // Jeżeli drugi użytkownik istnieje
            {
                var q = db.Pairs.Where(p => (p.User1.Id == curUser.Id && p.User2.Id == pair.UserId)
                                        || (p.User1.Id == pair.UserId && p.User2.Id == curUser.Id));

                if (q.Count() == 0) // Jeżeli nie istnieje taka para
                {
                    retPair = new Pair
                    {
                        User1 = curUser,
                        User2 = user2,
                        User1Status = pair.Status,
                        User2Status = Status.Unknown
                    };

                    db.Pairs.Add(retPair);
                    db.SaveChanges();

                    return Ok();
                }
                else // Jeżeli istnieje już taka para
                {
                    retPair = q.First();
                    
                    if (retPair.User1 == curUser) // Jeżeli para została utworzona przez zalogowanego użytkownika
                        return BadRequest("Przyznałeś już status temu użytkownikowi. Zmiana statusu jest niemożliwa!");

                    if(retPair.User2 == curUser) // Jeżeli para nie została utworzona przez zalogowanego użytkownika
                    {
                        if (retPair.User2Status == Status.Unknown) // Jeżeli status nie został jeszcze przynany
                        {
                            retPair.User2Status = pair.Status;

                            db.Entry(retPair).State = EntityState.Modified;
                            db.SaveChanges();

                            return Ok();
                        }
                        else // Jeżeli status został już przyznany
                            return BadRequest("Przyznałeś już status temu użytkownikowi. Zmiana statusu jest niemożliwa!");
                    }
                    return BadRequest("Coś poszło nie tak. Zadzwoń do programisty! :)");
                }
            }
            else // Jeżeli drugi użytkonik nie istnieje
            {
                return BadRequest("Id lajkowanego użytkownika jest niepoprawne lub nieistnieje!");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PairExists(int id)
        {
            return db.Pairs.Count(e => e.Id == id) > 0;
        }
    }
}