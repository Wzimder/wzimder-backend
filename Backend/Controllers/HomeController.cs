﻿using System.Web.Mvc;

namespace Backend.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToActionPermanent("Index", "Help");
        }
    }
}
