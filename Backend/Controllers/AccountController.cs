﻿using Backend.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using System;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Linq;

namespace Backend.Controllers
{
    [Authorize]
    [RoutePrefix("api/account")]
    public class AccountController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        /// <summary>
        /// Dane użytkownika
        /// </summary>
        [HttpGet]
        [Route("")]
        public AccountInfoViewModel GetAccoutInfo()
        {
            var userId = User.Identity.GetUserId();
            var user = db.Users.Single(u => u.Id == userId);

            return new AccountInfoViewModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Birthdate = user.Birthdate,
                Gender = user.Gender,
                Description = user.Description,
                ProfilePhotoId = user.ProfilePhoto
            };
        }
                
        // POST api/account/login
        /// <summary>
        /// Logowanie, TYLKO Content-Type: application/x-www-form-urlencoded
        /// </summary>
        /// <returns>Token</returns>
        [Route("login")]
        [ResponseType(typeof(LoginViewModel))]
        public IHttpActionResult Login([FromBody]LoginBindingModel model)
        {// tylko dla help page
            return Ok(new LoginViewModel());
        }

        // POST api/account/logout
        // [ApiExplorerSettings(IgnoreApi = true)]
        /// <summary>
        /// opcjonalne, aby się wylogować należy "zapomnieć" token
        /// </summary>
        /// <returns></returns>
        [Route("logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // POST api/account/register
        /// <summary>
        /// Rejestracja | wymagane w hasle: dlugosc >= 6, znaki specjalne, cyfry, male litery, duze litery
        /// </summary>
        [AllowAnonymous]        
        [Route("register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser()
            {
                UserName = model.Email,
                Email = model.Email,
                Gender = model.Gender,
                Birthdate = model.Birthdate,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Description = model.Description
            };

            user.ProfilePhoto = 0;
            
            Gender gender = model.Gender == Gender.Female ? Gender.Male : Gender.Female;

            user.SearchOptions = new SearchOptions { Gender = gender, MinAge = 18, MaxAge = 30, MaxDistanceKM = 50 };

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }


        // POST api/account/location
        /// <summary>
        /// ustawienie lokacji,
        /// przyklad formatu: "52.222795;22.979593", "6,784263;-58,187755", "52,222795;22.979593" GDZIE PIERWSZY PARAMETR (PRZED ŚREDNIKIEM) TO SZEROKOSC A DRUGI (PO ŚREDNIKU) DLUGOSC GEOGRAFICZNA
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("location")]
        //[AllowAnonymous]
        //Konrad Żyliński's comment
        //do przetestowania czy user sie update'uje, ja nie mam mozliwosci
        public IHttpActionResult SetLocation(LocationBindingModel location)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(location.Location, @"^-?\d+[.,]\d+[;]-?\d+[.,]\d+$"))
            {
                var t = User.Identity.GetUserId();
                var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
                var manager = new ApplicationUserManager(store);
                var user = manager.FindById(t);
                user.Location = location.Location;
                manager.Update(user);
                return Ok();
            }

            return BadRequest("Zly format");
        }

        /// <summary>
        /// Ustawienie zdjęcia profilowego
        /// </summary>
        /// <param name="id">Id zdjecia</param>
        //[HttpPost]
        //[Route("profilephoto/{id}")]
        //public IHttpActionResult SetProfilePhoto(int id)
        //{
        //    var userId = User.Identity.GetUserId();           
        //    var photo = db.Photos.Where(p => p.Id == id && p.User.Id == userId).FirstOrDefault();
        //    if (photo == null)
        //    {
        //        return BadRequest("Uzytkownik nie posiada zdjecia o id: " + id);
        //    }
        //    var user = db.Users.Find(userId);
        //    user.ProfilePhoto = id;
        //    db.SaveChanges();
        //    return Ok();
        //}

        /// <summary>
        /// Edycja opisu/zainteresowan. Zmiana imienia, nazwiska i daty urodzenia jest niedostepna
        /// </summary>
        /// <param name="description">opis/zainteresowania</param>
        [HttpPost]
        [Route("description")]
        public IHttpActionResult SetDescription(DescriptionBindingModel description)
        {
            if (!ModelState.IsValid || description == null)
            {
                return BadRequest();
            }

            var userId = User.Identity.GetUserId();
            var user = db.Users.Find(userId);

            user.Description = description.Description;
            db.SaveChanges();

            return Ok();
        }

        // POST api/account/changepassword
        /// <summary>
        /// Zmiana hasla
        /// </summary>
        [Route("changepassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }        

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    var i = 1;
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError(i.ToString(), error);
                        i = i + 1;
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
