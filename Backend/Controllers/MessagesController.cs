﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Backend.Models;
using Microsoft.AspNet.Identity;
using System.Net.Http;
using System.Net;
using System.Data.Entity;
using System;

namespace Backend.Controllers
{
    [Authorize]
    [Route("api/messages")]
    public class MessagesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// pobranie nowych wiadomości
        /// </summary>
        /// <returns></returns>
        public ICollection<MessageViewModel> GetMessages()
        {
            List<MessageViewModel> MessageList = new List<MessageViewModel>();
            var idUser = User.Identity.GetUserId();

            var m = db.Messages.Include(p=>p.Recipient).Include(p=>p.Sender).Where(p => p.Recipient.Id == idUser && p.Unread == true);

            foreach (var p in m)
            {
                MessageList.Add(new MessageViewModel
                {
                    SenderId = p.Sender.Id,
                    Text = p.Text,
                    TimeUtc = p.TimeUtc
                });
                
                p.Unread = false;
                
            }
            db.SaveChanges();
            return MessageList;
        }
        /// <summary>
        /// pobranie nowych wiadomości od konkretnego użytkownika
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/messages/{id}")]
        public ICollection<MessageViewModel> GetUserMessages(string id)
        {
            List<MessageViewModel> MessageList = new List<MessageViewModel>();
            ApplicationUser user = db.Users.Find(id);
            var idUser = User.Identity.GetUserId();

            if (user == null)
            {
                var res = new HttpResponseMessage(HttpStatusCode.BadRequest);
                res.Content = new StringContent("Nie ma takiego uzytkownika");
                throw new HttpResponseException(res);
            }

            var m = db.Messages.Include(p => p.Recipient).Include(p => p.Sender).Where(p => p.Sender.Id == user.Id && p.Recipient.Id == idUser && p.Unread == true);

            foreach (var p in m)
            {
                MessageList.Add(new MessageViewModel
                {
                    SenderId = p.Sender.Id,
                    Text = p.Text,
                    TimeUtc = p.TimeUtc
                });

                p.Unread = false;
                
            }
            db.SaveChanges();
            return MessageList;
        }
        
        /// <summary>
        /// wysłanie wiadomości do jakiegoś użytkownika
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public IHttpActionResult PostMessage(MessageBindingModel message)
        {
            ApplicationUser user = db.Users.Find(User.Identity.GetUserId());
            ApplicationUser userRecipient = db.Users.Find(message.RecipientId);
            Message messag;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userRecipient != null) //jeżeli istnieje odbiorca
            {
                var m = db.Pairs.Where(p => 
                    (p.User1.Id == user.Id && p.User2.Id == userRecipient.Id 
                    && p.User1Status == Status.Like && p.User2Status == Status.Like)

                    || (p.User1.Id == userRecipient.Id && p.User2.Id == user.Id
                    && p.User1Status == Status.Like && p.User2Status == Status.Like));

                if (m.Count() != 0) //jeżeli istnieje taka para
                {
                    messag = new Message
                    {
                        Text = message.Text,
                        Recipient = userRecipient,
                        Sender = user,
                        Unread = true,
                        TimeUtc = DateTime.UtcNow
                    };

                    db.Messages.Add(messag);
                    db.SaveChanges();
                }
                else
                {
                    return BadRequest("nie istnieje taka para");
                }
            }
            else
            {
                return BadRequest("nie istnieje odbiorca");
            }
            

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MessageExists(int id)
        {
            return db.Messages.Count(e => e.Id == id) > 0;
        }
    }
}