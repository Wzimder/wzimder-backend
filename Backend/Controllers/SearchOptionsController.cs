﻿using Backend.Models;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Web.Http;

namespace Backend.Controllers
{
    [Authorize]
    public class SearchOptionsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/searchoptions
        /// <summary>
        /// pobranie ustawien wyszukiwania
        /// </summary>
        /// <returns></returns>
        [Route("api/searchoptions")]
        public SearchOptionsViewModel GetSearchOptions()
        {
            var id = User.Identity.GetUserId();
            var user = db.Users.Find(id);
            var so = user.SearchOptions;
            var sovm = new SearchOptionsViewModel();

            sovm.MaxAge = so.MaxAge;
            sovm.MinAge = so.MinAge;
            sovm.Gender = so.Gender;
            sovm.MaxDistanceKM = so.MaxDistanceKM;

            return sovm;
        }

        // PUT: api/searchoptions
        /// <summary>
        /// zmiana ustawien wyszukiwania
        /// </summary>
        [Route("api/searchoptions")]        
        public IHttpActionResult PutSearchOptions(SearchOptionsBindingModel searchOptions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var id = User.Identity.GetUserId();
            var user = db.Users.Find(id);
            var so = user.SearchOptions;

            if (!(searchOptions.MaxAge >= searchOptions.MinAge))
            {
                return BadRequest("Minimalny wiek musi być niższy bądź równy maksymalnemu!");

            }
            if (!((searchOptions.Gender.Equals(Gender.Male)) | (searchOptions.Gender.Equals(Gender.Female)) | (searchOptions.Gender.Equals(Gender.Unknown))))
            {
                return BadRequest("Płeć musi być jedną z wartości: 0=Male, 1=Female, 2=Unknown");
            }
            if (!(searchOptions.MaxDistanceKM > 0))
            {
                return BadRequest("Maksymalna odległość nie może być liczbą ujemną!");
            }
            so.MaxAge = searchOptions.MaxAge;
            so.MinAge = searchOptions.MinAge;
            so.Gender = searchOptions.Gender;
            so.MaxDistanceKM = searchOptions.MaxDistanceKM;
            db.SaveChanges();
            return Ok();
        }        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SearchOptionsExists(string id)
        {
            return db.SearchOptions.Count(e => e.Id == id) > 0;
        }
    }
}